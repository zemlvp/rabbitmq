﻿using Core;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Text.Json;
using System.Threading;

namespace Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var connection = RabbitMQConnection.GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "homework",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                var usersWithoutEmailCount = 0;

                for (var i = 0; i < 1000; i++)
                {
                    var user = GetRandomUser();

                    Console.WriteLine(user.ToString());

                    if (user.email == "")
                        usersWithoutEmailCount++;

                    string json = JsonSerializer.Serialize(user);
                    var body = Encoding.UTF8.GetBytes(json);

                    channel.BasicPublish(exchange: "",
                                         routingKey: "homework",
                                         basicProperties: null,
                                         body: body);

                    //Thread.Sleep(1000);
                }

                Console.WriteLine($"Число юзеров без email: {usersWithoutEmailCount}");
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static User GetRandomUser() {
            Random rnd = new Random();

            var names = new string[] { "Вася", "Петя", "Коля"};
            var emails = new string[] { "mail1@ya.ru", "mail2@ya.ru", "mail3@ya.ru", "" };

            return new User { name = names[rnd.Next(names.Length)],
                age = rnd.Next(10,90),
                email = emails[rnd.Next(emails.Length)] };
        }
    }
}
