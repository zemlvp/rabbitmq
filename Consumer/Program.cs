﻿using Core;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var connection = RabbitMQConnection.GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "homework",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var json = Encoding.UTF8.GetString(body.ToArray());
                    User user = JsonSerializer.Deserialize<User>(json);
                 
                    Console.WriteLine(user.ToString());

                    if (user.email != "")
                        channel.BasicAck(ea.DeliveryTag, false);
                };

                channel.BasicConsume(queue: "homework", autoAck: false, consumer: consumer);

                Console.WriteLine("Press any key");
                Console.ReadLine();

            }
        }
    }
}
