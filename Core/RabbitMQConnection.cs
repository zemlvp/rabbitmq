﻿using RabbitMQ.Client;
using Microsoft.Extensions.Configuration;

namespace Core
{
    public static class RabbitMQConnection
    {
        public static IConnection GetRabbitConnection()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var appConfiguration = builder.Build();
            var rabbitSettingsSection = appConfiguration.GetSection("RabbitSettings");

            var factory = new ConnectionFactory()
            {
                UserName = rabbitSettingsSection.GetSection("UserName").Value,
                Password = rabbitSettingsSection.GetSection("Password").Value, 
                VirtualHost = rabbitSettingsSection.GetSection("VirtualHost").Value,
                HostName = rabbitSettingsSection.GetSection("HostName").Value
            };
            IConnection conn = factory.CreateConnection();

            return conn;
        }
    }
}