﻿namespace Core
{
    public class User
    {
        public string name { get; set; }
        public int age { get; set; }
        public string email { get; set; }

        public override string ToString()
        {
            return $"name={name}\n" +
                   $"age={age}\n"+
                   $"email={email}\n";
        }
    }
}
